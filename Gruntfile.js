/**
 * Created by NetGenes on 2016-03-06.
 */

module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        bump : {
            options : {
                commitFiles : ['-a'],
                pushTo : 'origin'
            }
        }
    });

    grunt.loadNpmTasks('grunt-bump');
};