# Promised Postgres #
Very simple wrapper for awesome node pg module.
## Installation ##
```shel
npm install --save promised-postgres
```
## Usage ##

```js
const Postgres = require('promised-postgres');
const ourDbInstance = new Postgres('connectionStringOrConfigObject');
var rollback; 
ourDbInstance
    .getNewClient()
    .then( function ( client) {
        rollback = client.roolback;
        return client.begin()
    })
    .then( function( client ) {
        
        return client.query('INSERT INTO something VALUES ($1) returning id',[ 'withParams' ]);
    })
    .then( function( result ) {
        
        return result.$client.query('UPDATE something_else SET a = $1',[ result.rows[0].id ])
    })
    .then( function ( result ) {
    
        //do something with result and commit
        result.$client.commit();
    })
    .then( function ( client ) {
        
        //!!dont forget release client
        client.done()
    })
    .catch(function( error) {
        
        rollback();
    });
```
## API
### Postgres
- Postgres.constructor( connectionConfig ) : Postgres
- Postgres.getNewClient( ) : Promise.&lt;Client&gt;
### Client
- Client.begin() : Promise.&lt;Client&gt;
- Client.commit() : Promise.&lt;Client&gt;
- Client.rollback() : Promise.&lt;Client&gt;
- Client.query( sql, params ) : Promise.&lt;Result&gt;
- Client.done( ) : void
### Result
result is a pg result object extended with one property:
- Result.$client : Client
