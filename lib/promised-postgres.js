/**
 * Created by jacek on 25.03.15.
 */

var pg      = require('pg');
var debug   = require('debug')('PromisedPostgres');

function Client( pgClient, done) {

    var client = pgClient;

    this.done  = function () {

        done();
    };

    this.end = function() {
        client.end();
    };

    this.on = function() {

        client.on.call( client, arguments );
    };

    this.query = ( sql, params ) => {

        return new Promise( function ( resolve, reject ) {

            client.query( sql, params , function (err, result) {

                if (err) {

                    debug('query error',err);
                    return reject(err);
                }

                result.$client = this;
                resolve( result );

            }.bind( this ));
        }.bind(this));
    };

    this.clientPromise = () => {

        return new Promise( function (resolve) {

            resolve( this )

        }.bind(this));
    };

    /**
     *
     * @returns {Promise.<Client>}
     */
    this.begin = function() {

        return this
            .query('BEGIN')
            .then( function( result ) {
                return this.clientPromise();
            }.bind(this));
    };

    /**
     *
     * @returns {Promise.<Client>}
     */
    this.commit = function() {
        return this
            .query('COMMIT')
            .then( function( ) {
                return this.clientPromise();
            }.bind(this));
    };

    /**
     *
     * @returns {Promise.<Client>}
     */
    this.rollback = function() {

        return this
            .query('ROLLBACK')
            .then( function( ) {
                return this.clientPromise();
            }.bind(this));
    };
}

function Postgres ( connectionConfiguration ) {

    /**
     *
     * @returns {Promise.<Client>}
     */
    this.getNewClient       = function () {

        return new Promise( function ( resolve, reject) {

            pg.connect(connectionConfiguration, function(err, client, done) {

                var newClient;

                if (err) {
                    debug('error fetching client from pool',err);
                    return reject(err);
                }

                newClient = new Client(client,done);
                resolve(newClient);
            });
        });
    };

    this.pool = pg.pool;
}

module.exports = Postgres;